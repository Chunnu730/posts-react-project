import React from "react";

export default class Error extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="card-box">
        <div className="error-card">
            Sorry, page not found !!!
        </div>
      </div>
    );
  }
}
