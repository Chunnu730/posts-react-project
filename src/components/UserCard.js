import React from "react";
import user_icon from "../images/user-icon.png";
import { Link } from "react-router-dom";
import Error from "./Error";
import mail from "../images/mail.png";
import location from "../images/location.jpeg";
import phone from "../images/phone.png";
import back_icon from "../images/back-icon.jpeg";
export default class UserCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      item: [],
      isDataLoaded: false,
      flag: false,
    };
  }

  componentDidMount() {
    fetch(
      "https://jsonplaceholder.typicode.com/users/" + this.props.match.params.id
    )
      .then((res) => res.json())
      .then((json) => {
        this.setState({
          item: json,
          isDataLoaded: true,
          cssStyle: {
            display: "none",
          },
        });
      });
  }
  render() {
    const { isDataLoaded, item } = this.state;
    console.log(item);
    if (!isDataLoaded) {
      return <div className="loader"></div>;
    } else if (JSON.stringify(item) === "{}") {
      return <Error />;
    }

    return (
      <>
        <div className="card-box">
          <Link to="/">
            <img src={back_icon} alt="" className="back-icon" />
          </Link>
          <session className="card">
            <div>
              <img src={user_icon} alt="" className="user-icon" />
            </div>
            <div className="user-name">
              <p>{item.username}</p>
            </div>
            <div className="inner-div">
              <p>{item.name}</p>
            </div>
            <div className="inner-div">
              <img src={mail} alt="" className="small-icon" />
              {item.email}
            </div>
            <div className="inner-div">
              <img src={phone} alt="" className="small-icon" />
              {item.phone}
            </div>
            <div className="inner-div">
              <p>
                {" "}
                Website: <Link>{item.website}</Link>
              </p>
            </div>
            <div className="inner-div">
              <img src={location} alt="" className="small-icon" />
              <p>
                {" "}
                {item.address.street}, {item.address.suite}, {item.address.city}
                , ({item.address.zipcode})
              </p>
            </div>
          </session>
        </div>
      </>
    );
  }
}
