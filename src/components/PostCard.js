import React from "react";
import user_icon from "../images/user-icon.png";
import comment_icon from "../images/comment-icon.png";
import { Link } from "react-router-dom";
import Error from "./Error";
import Comment from "./Comment";
import back_icon from "../images/back-icon.jpeg";
export default class PostCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      postsData: [],
      isDataLoaded: false,
    };
  }
  async componentDidMount() {
    const posts = await fetch(
      "https://jsonplaceholder.typicode.com/posts/" + this.props.match.params.id
    );
    const postJson = await posts.json();
    const users = await fetch("https://jsonplaceholder.typicode.com/users");
    const userJson = await users.json();
    this.setState({
      postsData: postJson,
      usersData: userJson,
      isDataLoaded: true,
    });
  }
  getUserName = (usersData, id) => {
    const data = usersData.find((element) => element.id === id);
    return data.username;
  };
  render() {
    const { isDataLoaded, postsData, usersData } = this.state;
    if (!isDataLoaded) {
      return <div className="loader"></div>;
    } else if (JSON.stringify(postsData) === "{}") {
      return <Error />;
    } else {
      return (
        <>
          <div className="card-box">
            <Link to="/">
              <img src={back_icon} alt="" className="back-icon" />
            </Link>
            <session className="title-card">
              <div className="heading">
                <div>
                  <img src={user_icon} alt="" className="user-icon" />
                </div>
                <div className="title">
                  <div className="user-name">
                    <Link
                      to={`/user/${postsData.userId}`}
                      className="user-link"
                    >
                      @{this.getUserName(usersData, postsData.userId)}
                    </Link>
                  </div>
                  <p style={{ color: "black" }}>{postsData.title}</p>
                </div>
              </div>
              <div className="post-body">
                <p>{postsData.body}</p>
              </div>
              <div className="Link">
              <img src={comment_icon} alt="" className="comment-icon" />
              <span className="post-span">All comments below</span>
              </div>
            </session>
          </div>
          <Comment id={postsData.id} />
        </>
      );
    }
  }
}
