import React from "react";
import { Link } from "react-router-dom";

export default class NavBar extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="nav-box">
        <nav>
          <Link to="/" className="link">Posts</Link>
        </nav>
      </div>
    );
  }
}