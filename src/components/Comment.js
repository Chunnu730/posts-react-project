import React from "react";
import user_icon from "../images/user-icon.png";
import Error from "./Error";
export default class Comment extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      isDataLoaded: false,
    };
  }
  componentDidMount() {
    fetch(
      "https://jsonplaceholder.typicode.com/posts/" +
        this.props.id +
        "/comments"
    )
      .then((res) => res.json())
      .then((json) => {
        this.setState({
          data: json,
          isDataLoaded: true,
        });
      });
  }
  render() {
    const { isDataLoaded, data} = this.state;
    if (!isDataLoaded) {
      return <div className="loader"></div>;
    } else if (data.length === 0) {
      return <Error />;
    } else {
      return (
        <>
          <div className="card-box">
            {data.map((item) => (
              <>
                <session className="comment-card">
                  <div className="heading">
                    <div>
                      <img src={user_icon} alt="" className="user-icon" />
                    </div>
                    <div className="title">
                      <p>@{item.name}</p>
                    </div>
                  </div>
                  <div className="comment-body">
                    <p>{item.body}</p>
                  </div>
                </session>
              </>
            ))}
          </div>

        </>
      );
    }
  }
}
