import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import PostCard from "./components/PostCard";
import Comment from "./components/Comment";
import UserCard from "./components/UserCard";
import Home from "./components/Home";
import "./index.css";

export default class App extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/post/:id" component={PostCard} />
          <Route exact path="/user/:id" component={UserCard} />
          <Route exact path="/comments/:id" component={Comment} />
        </Switch>
      </Router>
    );
  }
}
