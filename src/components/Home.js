import React from "react";
import user_icon from "../images/user-icon.png";
import comment_icon from "../images/comment-icon.png";
import { Link } from "react-router-dom";
export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      postsData: [],
      usersData: [],
      isDataLoaded: false,
    };
  }
  async componentDidMount() {
    const posts = await fetch("https://jsonplaceholder.typicode.com/posts");
    const postJson = await posts.json();
    const users = await fetch("https://jsonplaceholder.typicode.com/users");
    const userJson = await users.json();
    this.setState({
      postsData: postJson,
      usersData: userJson,
      isDataLoaded: true,
    });
  }
  getUserName = (usersData, id) => {
    const data = usersData.find((element) => element.id === id);
    return data.username;
  };
  render() {
    const { isDataLoaded, postsData, usersData } = this.state;
    if (!isDataLoaded) {
      return <div className="loader"></div>;
    } else {
      return (
        <>
          <div className="card-box">
            {postsData.map((item) => (
              <>
                <session className="title-card">
                  <div className="heading">
                    <div>
                      <img src={user_icon} alt="" className="user-icon" />
                    </div>
                    <div className="title">
                      <div className="user-name">
                        <Link to={`/user/${item.userId}`} className="user-link">
                          @{this.getUserName(usersData, item.userId)}
                        </Link>
                      </div>
                      <p style={{ color: "black" }}>{item.title}</p>
                    </div>
                  </div>
                  <Link to={`/post/${item.id}`} className="user-link">
                    <div className="post-body">
                      <p>{item.body}</p>
                    </div>
                  </Link>
                  <Link className="Link" to={`/post/${item.id}`}>
                    <img src={comment_icon} alt="" className="comment-icon" />
                    <span className="span">comments</span>
                  </Link>
                </session>
              </>
            ))}
          </div>
        </>
      );
    }
  }
}
